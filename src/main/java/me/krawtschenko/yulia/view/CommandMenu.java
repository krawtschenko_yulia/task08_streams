package me.krawtschenko.yulia.view;

public class CommandMenu extends View{
    public CommandMenu() {
        super();

        MenuItem add = new MenuItem(MenuOption.ADD,
                "Add");
        super.menu.put(MenuOption.ADD, add);

        MenuItem subtract = new MenuItem(MenuOption.SUBTRACT,
                "Subtract");
        super.menu.put(MenuOption.SUBTRACT, subtract);

        MenuItem multiply = new MenuItem(MenuOption.MULTIPLY,
                "Multiply");
        super.menu.put(MenuOption.MULTIPLY, multiply);

        MenuItem zeroMultiply = new MenuItem(MenuOption.MULTIPLY_BY_ZERO,
                "Multiply by zero");
        super.menu.put(MenuOption.MULTIPLY_BY_ZERO, zeroMultiply);

        MenuItem back = new MenuItem(MenuOption.BACK_TO_MAIN_MENU,
                "Go back to main menu");
        super.menu.put(MenuOption.BACK_TO_MAIN_MENU, back);
    }

    public int readInt() {
        return super.input.nextInt();
    }

    public String readCommand() {
        return super.input.nextLine().trim().split("\\s", 1)[0];
    }
}
