package me.krawtschenko.yulia.view;

import java.util.HashMap;
import java.util.Map;

public enum CommandOption {
    ADD("1"),
    SUBTRACT("2"),
    MULTIPLY("3"),
    MULTIPLY_BY_ZERO("4"),
    BACK_TO_MAIN_MENU("5");

    private final String key;

    CommandOption(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    private static final Map<String, CommandOption> table = new HashMap<>();

    static {
        for (CommandOption option : CommandOption.values()) {
            table.put(option.getKey(), option);
        }
    }

    public static CommandOption get(String key) {
        return table.get(key);
    }
}
