package me.krawtschenko.yulia.view;

import java.util.Map;

public class ApplicationView<T> extends View{
    public ApplicationView() {
        super();

        MenuItem findMaxOfThreeIntegers = new MenuItem(MenuOption.FIND_MAX_OF_THREE,
                "Find max of three integers");
        super.menu.put(MenuOption.FIND_MAX_OF_THREE, findMaxOfThreeIntegers);

        MenuItem findMedianOfThreeIntegers = new MenuItem(MenuOption.FIND_MEDIAN_OF_THREE,
                "Find median value");
        super.menu.put(MenuOption.FIND_MEDIAN_OF_THREE, findMedianOfThreeIntegers);

        MenuItem executeUserCommand = new MenuItem(MenuOption.EXECUTE_USER_COMMAND,
                "Execute command (calculations)");
        super.menu.put(MenuOption.EXECUTE_USER_COMMAND, executeUserCommand);

        MenuItem operateOnIntegers = new MenuItem(MenuOption.OPERATE_ON_INTEGERS,
                "See integer stream operations");
        super.menu.put(MenuOption.OPERATE_ON_INTEGERS, operateOnIntegers);

        MenuItem operateOnStrings = new MenuItem(MenuOption.OPERATE_ON_STRINGS,
                "See string stream operations");
        super.menu.put(MenuOption.OPERATE_ON_STRINGS, operateOnStrings);
    }

    public int readInt() {
        return super.input.nextInt();
    }

    public double readDouble() {
        return super.input.nextDouble();
    }

    public String readLine() {
        return super.input.nextLine();
    }

    public void printMap(Map map) {
        map.forEach((key, value) -> {
            System.out.println(key + ": " + value);
        });
    }

    public void printLongArray(long[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }

}
