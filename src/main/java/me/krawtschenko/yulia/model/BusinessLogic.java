package me.krawtschenko.yulia.model;

import java.util.*;
import java.util.stream.*;

public class BusinessLogic {
    public int getMaxInt(int first, int second, int third) {
        TriFunction findMaxInt = (one, two, three) -> {
            if (one > two && two > three) {
                return one;
            } else if (one < two && two > three) {
                return two;
            } else {
                return three;
            }
        };

        return findMaxInt.execute(first, second, third);
    }

    public int getMedian(int first, int second, int third) {
        TriFunction findMedianInt = (one, two, three) -> {
            if (one > two && one < three) {
                return one;
            } else if (one < two && one > three) {
                return one;
            } else if (two < one && two > three) {
                return two;
            } else if (two > one && two < three) {
                return two;
            } else {
                return three;
            }
        };

        return findMedianInt.execute(first, second, third);
    }

    public double add(double one, double two) {
        Calculator calculator = new Calculator();
        MathematicalOperation add = new Addition(one, two);

        double sum = calculator.performCalculation(add);

        return sum;
    }

    public double subtract(double one, double two) {
        Calculator calculator = new Calculator();
        Calculation calculation = new Calculation(one, two);

        double diff = calculator.performCalculation(calculation::subtract);

        return diff;
    }

    public double multiplyByZero(double one) {
        Calculator calculator = new Calculator();

        double zeroProduct = calculator
                .performCalculation(new MathematicalOperation() {
            @Override
            public double calculate() {
                return 0;
            }
        });

        return zeroProduct;
    }

    public double multiply(double one, double two) {
        Calculator calculator = new Calculator();
        Calculation mult = new Calculation(one, two);

        double product = calculator.performCalculation(() -> mult.multiply());

        return product;
    }

    public long[] generateRandomNumbers() {
        Random rand = new Random();

        LongStream numbers = rand.longs(11);

        return numbers.toArray();
    }

    public double countAvg(long[] nums) {
        return (double) Arrays.stream(nums).sum() / nums.length;
    }

    public OptionalDouble countAvgBuiltIn(LongStream nums) {
        return nums.average();
    }

    public OptionalLong findMin(LongStream nums) {
        return nums.min();
    }

    public OptionalLong findMax(LongStream nums) {
        return nums.max();
    }

    private int sumUp(int[] nums) {
        return Arrays.stream(nums).reduce(0, Integer::sum);
    }

    public long sumUpBultIn(double average, long[] nums) {
        return Arrays.stream(nums).sum();
    }

    public long countBiggerThanAvg(long[] nums) {
        OptionalDouble avg = Arrays.stream(nums).average();
        if (!avg.isPresent()) {
            return 0;
        }
        return Arrays.stream(nums).filter(n -> n > avg.getAsDouble()).count();
    }

    public String[] splitIntoWords(String line) {
        return line.trim().split("\\s");
    }

    public long countUniqueWords(String[] words) {
        return Arrays.stream(words).distinct().count();
    }

    public List<String> sortDistinct(String[] words) {
        return Arrays.stream(words)
                .distinct().sorted().collect(Collectors.toList());
    }

    public Map<String, Integer> countWordOccurrences(String[] words) {
        Map<String, Integer> occurrences = new HashMap<>();

        Arrays.stream(words).forEach(word -> {
            if (occurrences.containsKey(word)) {
                int count = occurrences.get(word) + 1;
                occurrences.put(word, count);
            } else {
                occurrences.put(word, 1);
            }
        });

        return occurrences;
    }

    public Map<Character, Integer> countCharOccurrences(String str) {
        Map<Character, Integer> occurrences = new HashMap<>();

        str.chars().mapToObj(c -> (char) c)
                .filter(c -> c < 'A' || c > 'Z')
                .forEach(c -> {
                if (occurrences.containsKey(c)) {
                    int count = occurrences.get(c) + 1;
                    occurrences.put(c, count);
                } else {
                    occurrences.put(c, 1);
                }
        });

        return occurrences;
    }
}
