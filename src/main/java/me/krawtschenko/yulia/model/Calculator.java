package me.krawtschenko.yulia.model;

import java.util.ArrayList;

public class Calculator {
    private final ArrayList<MathematicalOperation> calculations
            = new ArrayList<>();

    public double performCalculation(MathematicalOperation operation) {
        calculations.add(operation);

        return operation.calculate();
    }
}
