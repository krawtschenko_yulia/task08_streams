package me.krawtschenko.yulia.model;

@FunctionalInterface
public interface TriFunction {
    public int execute(int first, int second, int third);
}
