package me.krawtschenko.yulia.model;

public class Addition implements MathematicalOperation {
    private double one;
    private double two;

    public Addition(double one, double two) {
        this.one = one;
        this.two = two;
    }

    public Addition(double one) {
        this.one = one;
        two = 0;
    }

    public Addition() {
        one = 0;
        two = 0;
    }

    @Override
    public double calculate() {
        return one + two;
    }
}
