package me.krawtschenko.yulia.model;

@FunctionalInterface
public interface MathematicalOperation {
    double calculate();
}
