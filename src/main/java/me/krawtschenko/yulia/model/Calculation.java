package me.krawtschenko.yulia.model;

public class Calculation {
    private double one;
    private double two;

    public Calculation(double one, double two) {
        this.one = one;
        this.two = two;
    }

    public double add() {
        Addition add = new Addition(one, two);

        return add();
    }

    public double subtract() {
        return one - two;
    }

    public double multiply() {
        return one * two;
    }

    public double divide() {
        return one / two;
    }
}
