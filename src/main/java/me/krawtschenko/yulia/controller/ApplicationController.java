package me.krawtschenko.yulia.controller;

import me.krawtschenko.yulia.model.BusinessLogic;
import me.krawtschenko.yulia.view.ApplicationView;
import me.krawtschenko.yulia.view.CommandMenu;
import me.krawtschenko.yulia.view.MenuAction;
import me.krawtschenko.yulia.view.MenuOption;

import java.util.Arrays;
import java.util.OptionalDouble;
import java.util.OptionalLong;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ApplicationController {
    private BusinessLogic model;
    private ApplicationView view;

    private MenuAction findMaxOfThreeIntegers;
    private MenuAction findMedianOfThreeIntegers;
    private MenuAction executeUserCommand;
    private MenuAction operateOnIntegers;
    private MenuAction operateOnStrings;
    private MenuAction quit;

    public ApplicationController() {
        model = new BusinessLogic();
        view = new ApplicationView();

        findMaxOfThreeIntegers = new MenuAction() {
            @Override
            public void execute() {
                int[] three = new int[3];

                view.printMessage("Enter 3 integers: ");
                for (int i = 0; i < three.length; i++) {
                    three[i] = view.readInt();
                }
                view.readLine();

                int max = model.getMaxInt(three[0], three[1], three[2]);

                view.printMessage("The largest number is " + max);
            }
        };
        view.addMenuItemAction(MenuOption.FIND_MAX_OF_THREE, findMaxOfThreeIntegers);

        findMedianOfThreeIntegers = new MenuAction() {
            @Override
            public void execute() {
                int[] three = new int[3];

                view.printMessage("Enter 3 integers: ");
                for (int i = 0; i < three.length; i++) {
                    three[i] = view.readInt();
                }
                view.readLine();

                int median = model.getMedian(three[0], three[1], three[2]);

                view.printMessage("The median is " + median);
            }
        };
        view.addMenuItemAction(MenuOption.FIND_MEDIAN_OF_THREE, findMedianOfThreeIntegers);

        executeUserCommand = new MenuAction() {
            @Override
            public void execute() {
                CommandMenu menu = new CommandMenu();

                menu.addMenuItemAction(MenuOption.ADD, () -> {
                    view.printMessage("Enter two numbers: ");
                    double one = view.readDouble();
                    double two = view.readDouble();

                    view.printMessage("Result: ");
                    view.print(model.add(one, two));
                });

                menu.addMenuItemAction(MenuOption.SUBTRACT, () -> {
                    view.printMessage("Enter two numbers: ");
                    double one = view.readDouble();
                    double two = view.readDouble();

                    view.printMessage("Result: ");
                    view.print(model.subtract(one, two));
                });

                menu.addMenuItemAction(MenuOption.MULTIPLY, () -> {
                    view.printMessage("Enter two numbers: ");
                    double one = view.readDouble();
                    double two = view.readDouble();

                    view.printMessage("Result: ");
                    view.print(model.multiply(one, two));
                });

                menu.addMenuItemAction(MenuOption.MULTIPLY_BY_ZERO, () -> {
                    view.printMessage("Enter a number: ");
                    double one = view.readDouble();

                    view.printMessage("Result: ");
                    view.print(model.multiplyByZero(one));
                });

                menu.addMenuItemAction(MenuOption.BACK_TO_MAIN_MENU, () -> {
                    startMenu();
                });

                view.printMessage("CHOOSE COMMAND: ");
                menu.displayMenu();
            }
        };
        view.addMenuItemAction(MenuOption.EXECUTE_USER_COMMAND, executeUserCommand);

        operateOnIntegers = new MenuAction() {
            @Override
            public void execute() {
                long[] numbers = model.generateRandomNumbers();
                view.printMessage("Generated numbers: ");
                view.printLongArray(numbers);

                double average = model.countAvg(numbers);
                view.printMessage("Average: ");
                view.print(average);

                OptionalLong min = model.findMin(Arrays.stream(numbers));
                view.printMessage("Min: ");
                view.print(min.getAsLong());

                OptionalLong max = model.findMax(Arrays.stream(numbers));
                view.printMessage("Max: ");
                view.print(max.getAsLong());

                long sum = model.sumUpBultIn(average, numbers);
                view.printMessage("Sum: ");
                view.print(sum);

                long biggerThanAverage = model.countBiggerThanAvg(numbers);
                view.printMessage("Number of elements larger than average: ");
                view.print(biggerThanAverage);
            }
        };
        view.addMenuItemAction(MenuOption.OPERATE_ON_INTEGERS,
                operateOnIntegers);

        operateOnStrings = new MenuAction() {
            @Override
            public void execute() {
                StringBuilder lines = new StringBuilder();
                String line;

                view.printMessage("Enter text: ");
                while (!(line = view.readLine()).isBlank()) {
                    lines.append(line);
                    lines.append(' ');
                }

                String text = lines.toString();

                String[] words = model.splitIntoWords(text);

                view.printMessage("Number of distinct words: ");
                view.print(model.countUniqueWords(words));

                view.printMessage("Sorted list of distinct words: ");
                view.print(model.sortDistinct(words).toString());

                view.printMessage("Word count: ");
                view.printMap(model.countWordOccurrences(words));

                view.printMessage("Character count (except upper case): ");
                view.printMap(model.countCharOccurrences(text));
            }
        };
        view.addMenuItemAction(MenuOption.OPERATE_ON_STRINGS, operateOnStrings);

        quit = new MenuAction() {
            @Override
            public void execute() {
                System.exit(0);
            }
        };
        view.addMenuItemAction(MenuOption.QUIT, quit);
    }

    public void startMenu() {
        view.displayMenu();
    }
}